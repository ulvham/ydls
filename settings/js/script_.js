function mf_deff() {
var divs = document.getElementsByTagName("input");
for (var i = 0; i < divs.length; i++) {
var node = divs[i];
//alert(divs[i].innerHTML);
var longpress = false;
var presstimer = null;
var longtarget = null;
var cancel = function(e) {
    if (presstimer !== null) {
        clearTimeout(presstimer);
        presstimer = null;
    }
    this.classList.remove("longpress");
};
var click = function(e) {
    if (presstimer !== null) {
        clearTimeout(presstimer);
        presstimer = null;
    }
    this.classList.remove("longpress");
    if (longpress) {
        return false;
    }
    //alert("press");
};

var start = function(e) {
    console.log(e);
    if (e.type === "click" && e.button !== 0) {
        return;
    }
    longpress = false;
    this.classList.add("longpress");
    presstimer = setTimeout(function() {
		editch(e.target.value);
        longpress = true;
    }, 800);
    return false;
};

node.addEventListener("mousedown", start);
node.addEventListener("touchstart", start);
node.addEventListener("click", click);
node.addEventListener("mouseout", cancel);
node.addEventListener("touchend", cancel);
node.addEventListener("touchleave", cancel);
node.addEventListener("touchcancel", cancel);

};
}

function myFunction() {

if (!document.getElementById('test_add_ch')){
var inp = document.createElement("input");
inp.setAttribute("type", "text");
inp.setAttribute("id", "test_add_ch");
inp.setAttribute("class", "chlabel");
inp.setAttribute("name", "channel_name");
document.getElementById("add_ch").appendChild(inp);
inp.focus();
} else {
document.getElementById('test_add_ch').remove();
}

if (!document.getElementById('test_add_ch_but')){
var but = document.createElement("button");
but.setAttribute("name", "fileini");
but.setAttribute("id", "test_add_ch_but");
but.setAttribute("style", "cursor: pointer;");
but.setAttribute("type", "submit");
var textnode = document.createTextNode("Добавить");
but.appendChild(textnode);
document.getElementById("add_ch_but").appendChild(but);
} else {
document.getElementById('test_add_ch_but').remove();
}
}
function editch(text_) {
if (!document.getElementById('test_edit_ch')){
var inp = document.createElement("input");
inp.setAttribute("type", "text");
inp.setAttribute("id", "test_edit_ch");
inp.setAttribute("class", "chlabel");
inp.setAttribute("name", "channel_name");
inp.setAttribute("value", text_);
document.getElementById("edit_ch1").appendChild(inp);
inp.focus();
var inp2 = document.createElement("input");
inp2.setAttribute("type", "text");
inp2.setAttribute("id", "test_edit_ch2");
inp2.setAttribute("class", "chlabel");
inp2.setAttribute("name", "channel_name_hide");
inp2.setAttribute("value", text_);
document.getElementById("edit_ch2").appendChild(inp2);
} else {
document.getElementById('test_edit_ch').remove();
document.getElementById('test_edit_ch2').remove();
}
if (!document.getElementById('test_edit_ch_but')){
var but = document.createElement("button");
but.setAttribute("name", "fileini");
but.setAttribute("id", "test_edit_ch_but");
but.setAttribute("style", "cursor: pointer;");
but.setAttribute("type", "submit");
but.setAttribute("value", "del");
var textnode = document.createTextNode("Удалить/Изменить");
but.appendChild(textnode);
document.getElementById("edit_ch_but").appendChild(but);
} else {
document.getElementById('test_edit_ch_but').remove();
}
}