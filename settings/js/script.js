function onmo(idd) {
   $("status").innerHTML = $(idd).innerHTML;
}

function onml(idd) {
   $("status").innerHTML = "";
}

function failureFunc(response) {
   $("status").innerHTML = "<i>Ошибка</i>";
}

function video_list_onload() {

//var divs = document.getElementsByTagName("input");
var divs = $$('div input');
for (var i = 0; i < divs.length; i++) {
var node = divs[i];
var longpress = false;
var presstimer = null;
var longtarget = null;
var cancel = function(e) {
    if (presstimer !== null) {
        clearTimeout(presstimer);
        presstimer = null;
    }
    this.classList.remove("longpress");
};
var click = function(e) {
    if (presstimer !== null) {
        clearTimeout(presstimer);
        presstimer = null;
    }
    this.classList.remove("longpress");
    if (longpress) {
        return false;
    }
    video_list(e.target.id)
};

var start = function(e) {
    console.log(e);
    if (e.type === "click" && e.button !== 0) {
        return;
    }
    longpress = false;
    this.classList.add("longpress");
    presstimer = setTimeout(function() {
		editch(e.target.id);
        longpress = true;
    }, 800);
    return false;
};

node.addEventListener("mousedown", start);
node.addEventListener("touchstart", start);
node.addEventListener("click", click);
node.addEventListener("mouseout", cancel);
node.addEventListener("touchend", cancel);
node.addEventListener("touchleave", cancel);
node.addEventListener("touchcancel", cancel);

};

 }

function video_list_more() {
	new Ajax.Request('/run', {
       method: 'post',
       parameters: 'fileini=' + $("morevalch").value + '&count=10&count_page=' + $("moreval").value,
       onSuccess: successFuncVl,
       onFailure:  failureFunc
    });
    $("moreval").value = parseInt($("moreval").value)+1;
}

function video_list(elem_) {
	new Ajax.Request('/run', {
       method: 'post',
       parameters: 'fileini=' + elem_ + '&count=10&count_page=1',
       onSuccess: successFuncVl,
       onFailure:  failureFunc
    });
    $('ch_list').fade({ duration:0.0, from:1.0, to:0.0 });
    $("morevalch").value = elem_;
    //$('video_list').fade({ duration:1.0, from:1.0, to:0.0 });
}

function video_load_list_() {
  var divs = document.getElementsByTagName("input");
  for (var i = 0; i < divs.length; i++) {
    if (divs[i].checked == true) {
    var node = divs[i].name;
    //alert(node);
    video_load(node);
  }
}
}

function video_load_list(elem_) {
  var timerId = setTimeout(function tick() {
  video_load(elem_);
  timerId = setTimeout(tick, 2000); // (*)
  }, 2000);

  //var intervalID = setInterval(function() { video_load(elem_) }, 3000)
  //clearInterval(intervalID)
}


function video_load(elem_) {
  //alert('['+elem_+']');
  new Ajax.Request('/download_percent', {
       method: 'post',
       parameters: 'procent=' + elem_,
       onSuccess: function(response) {
            //if (200 == response.status) {
                var container = $('dl_'+elem_);
                var content = response.responseText;
                container.update(content);
              //$("dl_"+elem_).innerHTML = response.responseText;
            // }
            }
    });
}



function successFuncVl(response) {
    if (200 == response.status) {
        //$("video_list").innerHTML = response.responseText;
        var container = $('video_list');
        var content = response.responseText;
        container.update(content);
        //$('video_list').appear();
    }
    //var container = $('notice');
    //var content = response.responseText;
    //container.update(content);
}





 function ch_list_edit() {
	new Ajax.Request('/edit_ch', {
       method: 'post',
       parameters: 'fileini=del&' + $("test_edit_ch").name + "=" + $("test_edit_ch").value + "&" + $("test_edit_ch2").name + "=" + $("test_edit_ch2").value,
       onSuccess: successFuncEdit,
       onFailure:  failureFunc
    });
    $("test_edit_ch").fade({ duration:3.0, from:1.0, to:0.0 });
    $("test_edit_ch_but").fade({ duration:3.0, from:1.0, to:0.0 });
}

 function successFuncEdit(response) {
    if (200 == response.status) {
		loadPage()
    }
 }

function ch_list_add() {
	new Ajax.Request('/add_ch', {
       method: 'post',
       parameters: $("test_add_ch").name + "=" + $("test_add_ch").value + '&fileini=',
       onSuccess: successFuncAdd,
       onFailure:  failureFunc
    });
    $("test_add_ch").fade({ duration:3.0, from:1.0, to:0.0 });
    $("test_add_ch_but").fade({ duration:3.0, from:1.0, to:0.0 });
}

 function successFuncAdd(response) {
    if (200 == response.status) {
		loadPage()
    }
 }

 function loadPage() {
 	new Ajax.Request('/runch', {
       method: 'post',
       onSuccess: successFuncCh,
       onFailure:  failureFunc
    });
    $("status").innerHTML = "";
	  $('status').show();
    $('statuspb').fade({ duration:0.0, from:0.0, to:0.0 });
    //$("status").innerHTML = "loading arts... ";
    //$('status').fade({ duration:3.0, from:1.0, to:0.0 });
 }

 function successFuncCh(response) {
    if (200 == response.status) {
        $("ch_list").innerHTML = response.responseText;
        //$('ch_list').show();
    }
    video_list_onload();
 }

 function retback() {
    $("video_list").innerHTML = ""
    $('ch_list').show();
 }

 function dl_video(param) {
    pars = "";
    var array = $$('div#vlist input');
    for(var i=0; i<array.length; i++){
         if (array[i].checked) {
            pars += array[i].value + "=" + array[i].value + "&";
        }
        
    }
    pars += param+"="+param
    // new Ajax.Request('/download', {
    //    method: 'post',
    //    parameters: pars,
    //    onSuccess: successFuncDl,
    //    onFailure:  failureFunc
    // });
    new Ajax.Request('/download', {
      method: 'post',
      parameters: pars,
      onSuccess: function(response) {
            if (200 == response.status) {
            	$("statuspb").innerHTML = "";
                $('statuspb').show();
                //$("status").innerHTML = "starting " + param;
                //$(param).pulsate();
                //$('status').fade({ duration:7.0, from:1.0, to:0.0 });
                //$("status").innerHTML = "";
                //$(param).Highlight({ startcolor: '#ffff99', endcolor: '#ffffff' });
var bar = new ProgressBar.Circle(statuspb, {
  strokeWidth: 6,
  easing: 'easeInOut',
  duration: 1400,
  color: '#009900',
  trailColor: '#eee',
  trailWidth: 1,
  svgStyle: null
});

bar.animate(1.0);  // Number from 0.0 to 1.0
$('statuspb').fade({ duration:3.0, from:1.0, to:0.0 });
     }
      },
      onFailure:  failureFunc
    });
 }
 function successFuncDl(response) {
    if (200 == response.status) {
        $("plus").pulsate();
    }
 }

function myFunction() {

var click = function(e) {
    ch_list_add();
};

if (!document.getElementById('test_add_ch')){
var inp = document.createElement("input");
inp.setAttribute("type", "text");
inp.setAttribute("id", "test_add_ch");
inp.setAttribute("class", "chlabel");
inp.setAttribute("name", "channel_name");
document.getElementById("add_ch").appendChild(inp);
inp.focus();
} else {
document.getElementById('test_add_ch').remove();
}

if (!document.getElementById('test_add_ch_but')){
var but = document.createElement("button");
but.setAttribute("name", "fileini");
but.setAttribute("id", "test_add_ch_but");
but.setAttribute("style", "cursor: pointer;");
//but.setAttribute("type", "submit");
but.setAttribute("type", "button");
but.addEventListener("click", click);
var textnode = document.createTextNode("Добавить");
but.appendChild(textnode);
document.getElementById("add_ch_but").appendChild(but);
} else {
document.getElementById('test_add_ch_but').remove();
}
}
function editch(text_) {
var click = function(e) {
    ch_list_edit();
};
if (!document.getElementById('test_edit_ch')){
var inp = document.createElement("input");
inp.setAttribute("type", "text");
inp.setAttribute("id", "test_edit_ch");
inp.setAttribute("class", "chlabel");
inp.setAttribute("name", "channel_name");
inp.setAttribute("value", text_);
document.getElementById("edit_ch1").appendChild(inp);
inp.focus();
var inp2 = document.createElement("input");
inp2.setAttribute("type", "text");
inp2.setAttribute("id", "test_edit_ch2");
inp2.setAttribute("class", "chlabel");
inp2.setAttribute("name", "channel_name_hide");
inp2.setAttribute("value", text_);
document.getElementById("edit_ch2").appendChild(inp2);
} else {
document.getElementById('test_edit_ch').remove();
document.getElementById('test_edit_ch2').remove();
}
if (!document.getElementById('test_edit_ch_but')){
var but = document.createElement("button");
but.setAttribute("name", "fileini");
but.setAttribute("id", "test_edit_ch_but");
but.setAttribute("style", "cursor: pointer;");
//but.setAttribute("type", "submit");
but.setAttribute("type", "button");
but.setAttribute("value", "del");
but.addEventListener("click", click);
var textnode = document.createTextNode("Удалить/Изменить");
but.appendChild(textnode);
document.getElementById("edit_ch_but").appendChild(but);
} else {
document.getElementById('test_edit_ch_but').remove();
}
}