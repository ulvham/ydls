<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Youtube Download</title>
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link rel="stylesheet" href="/css/style.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/prototype/1.7.3.0/prototype.js"></script>
<script src="/js/script.js"></script>
{{"\n"}}
{{range .Item}}
<script>
function myFunction() {
	var link = document.createElement('a');
	link.setAttribute('href','{{.UrlImg}}');
	link.setAttribute('download','{{.Value}}');
	onload=link.click();
}
</script>
</head>
<body>
<br><br><br>
<a class="href" href='../'>НАЗАД</a><br>
<a onclick="myFunction()" class="href" href='../'>Скачать Все</a>
<br><br><br>
<a class="href" href="{{.UrlImg}}">{{.Value}}</a><br>
</body>
</html>
{{end}}