// youtube_prog
package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	//"net"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"gopkg.in/cheggaaa/pb.v1"

	"bytes"
	"errors"

	"github.com/go-martini/martini"
	"github.com/otium/ytdl"
	"github.com/vaughan0/go-ini"
	"golang.org/x/net/html"
	"gopkg.in/telegram-bot-api.v4"
	//"github.com/go-telegram-bot-api/telegram-bot-api"
)

const (
	//runtmp  = `{{block "list" .}}{{"\n"}}{{range .}}{{println "" .}}{{end}}{{end}}`
	runtmp                = `{{define "list"}} {{join . "<br>"}}{{end}} `
	youtubeDateFormat     = "2006-01-02"
	youtubeBaseURL        = "https://www.youtube.com/watch"
	youtubeEmbededBaseURL = "https://www.youtube.com/embed/"
	youtubeVideoEURL      = "https://www.googleapis.com/youtube/v3/"
	youtubeVideoInfoURL   = "https://www.youtube.com/get_video_info"

	namefilech = "settings/channels"
	part       = "snippet%2CcontentDetails%2Cstatistics"
)

var (
	pageonload = 1
	def_count_ = "15"
	key        = ""
	keytel     = ""
	ini_file   = "settings/settings.ini"
	//chan_      = make(chan int64)
	//chan_ = make(map[string](chan string))
	chan_ = make(map[string]string)
)

type VideoInfo struct {
	ID             string    `json:"id"`
	Title          string    `json:"title"`
	Description    string    `json:"description"`
	DatePublished  time.Time `json:"datePublished"`
	Keywords       []string  `json:"keywords"`
	Author         string    `json:"author"`
	TimeStr        string
	Duration       time.Duration
	Paths          url.Values
	PathsAV        url.Values
	htmlPlayerFile string
}

type Paths struct {
	Path string
	Type string
	Part bool
	Res  string
}

type Query struct {
	Kind     string    `json:"kind"`
	Etag     string    `json:"etag"`
	PageInfo *PageInfo `json:"pageInfo"`
	Items    []Items_  `json:"items"`
}

type PageInfo struct {
	TotalResults   int `json:"totalResults"`
	ResultsPerPage int `json:"resultsPerPage"`
}

type ContentDetails struct {
	RelatedPlaylists *RelatedPlaylists `json:"relatedPlaylists"`
}

type RelatedPlaylists struct {
	Likes   string `json:"likes"`
	Uploads string `json:"uploads"`
}

type Items_ struct {
	Kind           string          `json:"kind"`
	Etag           string          `json:"etag"`
	Id             string          `json:"id"`
	ContentDetails *ContentDetails `json:"contentDetails"`
	Snippet        *SnippetV       `json:"snippet"`
	Statistics     *Stat           `json:"statistics"`
}

type Stat struct {
	SubscriberCount string `json:"subscriberCount"`
	VideoCount      string `json:"videoCount"`
}

func (slice *QueryV) AddItem(item ItemsV) []ItemsV {
	return append(slice.Items, item)
}

type QueryV struct {
	Kind          string    `json:"kind"`
	Etag          string    `json:"etag"`
	NextPageToken string    `json:"nextPageToken"`
	PageInfo      *PageInfo `json:"pageInfo"`
	Items         []ItemsV  `json:"items"`
}

type PageInfoV struct {
	TotalResults   int `json:"totalResults"`
	ResultsPerPage int `json:"resultsPerPage"`
}

type ItemsV struct {
	Kind    string    `json:"kind"`
	Etag    string    `json:"etag"`
	Id      string    `json:"id"`
	Snippet *SnippetV `json:"snippet"`
}

type SnippetV struct {
	PublishedAt  string      `json:"publishedAt"`
	ChannelId    string      `json:"channelId"`
	Title        string      `json:"title"`
	Description  string      `json:"description"`
	ChannelTitle string      `json:"channelTitle"`
	PlaylistId   string      `json:"playlistId"`
	Position     int         `json:"position"`
	ResourceId   ResourceIdV `json:"resourceId"`
	//Thumbnails   *Quality_standard `json:"thumbnails"`
	Thumbnails *Quality_medium `json:"thumbnails"`
}

type ResourceIdV struct {
	Kind    string `json:"kind"`
	VideoId string `json:"videoId"`
}

type Quality_default struct {
	Quality_Item Quality_Item `json:"default"`
}

type Quality_medium struct {
	Quality_Item Quality_Item `json:"medium"`
}

type Quality_high struct {
	Quality_Item Quality_Item `json:"high"`
}

type Quality_standard struct {
	Quality_Item Quality_Item `json:"standard"`
}

type Quality_maxres struct {
	Quality_Item Quality_Item `json:"maxres"`
}

type Quality_Item struct {
	Url    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

type Item_box struct {
	Name string
	Text string
}

type Items_box struct {
	Items []Item_box
}

type ItemVideo struct {
	Name    string
	Value   string
	Checked string
	Time    string
	Title   string
	UrlImg  string
	Pos     string
	Ico     string
	Id      string
	Subs    string
}

type PageItem struct {
	Item       []ItemVideo
	IcoGeneral string
}

func (slice Items_box) Len() int {
	return len(slice.Items)
}

func (slice Items_box) Less(i, j int) bool {
	return slice.Items[i].Name < slice.Items[j].Name
}

func (slice Items_box) Swap(i, j int) {
	slice.Items[i], slice.Items[j] = slice.Items[j], slice.Items[i]
}

func (slice *Items_box) AddItem(item Item_box) []Item_box {
	return append(slice.Items, item)
}

func write_channels_file() string {
	d1 := []byte("")
	ioutil.WriteFile("./"+namefilech, d1, 0777)
	return ""
}

func read_channels() []string {
	dat, _ := ioutil.ReadFile("./" + namefilech)
	new := strings.Replace(string(dat), "<ch>", "", -1)
	new = strings.TrimRight(new, "</ch>")
	return strings.Split(new, "</ch>")
}

func add_channel(text string) {
	for _, val := range read_channels() {
		if val == text {
			return
		}
	}
	if strings.TrimSpace(text) != "" {
		f, err := os.OpenFile("./"+namefilech, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		if _, err = f.WriteString("<ch>" + text + "</ch>"); err != nil {
			panic(err)
		}
	}
	return
}

func del_channel(text string, texth string) {
	dat, _ := ioutil.ReadFile("./" + namefilech)
	new := ""
	if text == texth {
		new = strings.Replace(string(dat), "<ch>"+text+"</ch>", "", 1)
	} else {
		new = strings.Replace(string(dat), "<ch>"+texth+"</ch>", "<ch>"+text+"</ch>", 1)
	}
	d1 := []byte(new)
	ioutil.WriteFile("./"+namefilech, d1, 0777)
	return

}

type Channels struct {
	Channels []ChannelItem
}

type ChannelItem struct {
	Title       string
	Id          string
	Upload      string
	Ico         string
	Subscribers string
	VideoCount  string
}

func ulist(data string) *Channels {
	res := Query{}
	json.Unmarshal([]byte(data), &res)
	var chstmp []ChannelItem
	var chs Channels
	for i := 0; i < len(res.Items); i++ {
		var newone ChannelItem
		newone.Title = res.Items[i].Snippet.Title
		newone.Id = res.Items[i].Id
		newone.Upload = res.Items[i].ContentDetails.RelatedPlaylists.Uploads
		newone.Ico = res.Items[i].Snippet.Thumbnails.Quality_Item.Url
		newone.Subscribers = res.Items[i].Statistics.SubscriberCount
		newone.VideoCount = res.Items[i].Statistics.VideoCount
		chstmp = append(chstmp, newone)
	}
	chs.Channels = chstmp
	return &chs
}

func ulist_pic_ch(data string) string {
	res := QueryV{}
	json.Unmarshal([]byte(data), &res)
	if res.PageInfo.TotalResults == 0 {
		return ""
	}
	//return res.Items[0].Snippet.Thumbnails.Quality_Item.Url
	return res.Items[0].Snippet.Thumbnails.Quality_Item.Url
}

func ulist_videos(data string) (QueryV, string, int, int) {
	res := QueryV{}
	json.Unmarshal([]byte(data), &res)
	return res, res.NextPageToken, res.PageInfo.ResultsPerPage, res.PageInfo.TotalResults
}

func ulistvideo(data string) QueryV {
	res := QueryV{}
	json.Unmarshal([]byte(data), &res)
	return res
}

func name_or_id(name string) string {
	responseid, _ := http.Get(youtubeVideoEURL + "channels?part=" + part + "&id=" + name + "&key=" + key)
	defer responseid.Body.Close()
	contentsid, _ := ioutil.ReadAll(responseid.Body)
	tmpid := ulist(string(contentsid)).Channels
	if len(tmpid) > 0 {
		return tmpid[0].Id
	}
	responsename, _ := http.Get(youtubeVideoEURL + "channels?part=" + part + "&forUsername=" + name + "&key=" + key)
	defer responsename.Body.Close()
	contentsname, _ := ioutil.ReadAll(responsename.Body)
	tmpname := ulist(string(contentsname)).Channels
	if len(tmpname) > 0 {
		return tmpname[0].Id
	}
	return ""
}

func info_ch(username string) *Channels {
	response, err := http.Get(youtubeVideoEURL + "channels?part=" + part + "&id=" + username + "&key=" + key)
	//response, err := http.Get(youtubeVideoEURL + "channels?part=" + part + "&forUsername=" + username + "&key=" + seckey)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		return ulist(string(contents))
	}
	var chs Channels
	return &chs
}

func info_video(videokey string) QueryV {
	var res QueryV
	response, err := http.Get("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + videokey + "&key=" + key)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		return ulistvideo(string(contents))

	}
	return res
}

func info_ch_(username string) string {
	response, err := http.Get("https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername=" + username + "&key=" + key)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		return ulist_pic_ch(string(contents))

	}
	return ""
}

func videos(ulist string, nextpage string, count_ string) QueryV {
	var res QueryV
	textresp := ""
	if nextpage == "" {
		textresp = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=" + count_ + "&playlistId=" + ulist + "&key=" + key
	} else {
		textresp = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=" + count_ + "&pageToken=" + nextpage + "&playlistId=" + ulist + "&key=" + key
	}

	response, err := http.Get(textresp)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		res, _, _, _ = ulist_videos(string(contents))
		return res
	}
	return res
}

func read_ini(fname string, sec string) ini.Section {
	//dir, _ := os.Getwd()
	//file, _ := ini.LoadFile(dir + "/" + fname)
	file, _ := ini.LoadFile(fname)
	return file[sec]
}

func load(sett string, null_ string) {
	ini_ := read_ini(sett, "settings")
	channels := strings.Split(ini_["channels"], "{!}")
	channels_name := strings.Split(ini_["channels_name"], "{!}")
	channels_last_day := strings.Split(ini_["channels_last_day"], "{!}")
	channels_b_w := strings.Split(ini_["channels_b_w"], "{!}")
	for i := 0; i < len(channels); i++ {
		if channels[i] != "" {
			response, err := http.Get(channels[i])
			if err != nil {
				fmt.Printf("%s", err)
				os.Exit(1)
			} else {
				defer response.Body.Close()
				tokenizer := html.NewTokenizer(response.Body)
				var (
					islink bool
					link   string
				)
				links := make(map[string]string)
			loop:
				for {
					tok := tokenizer.Next()
					switch tok {
					case html.ErrorToken:
						break loop
					case html.StartTagToken:
						tag, _ := tokenizer.TagName()
						if string(tag) == "a" {
							islink = true
							_, linkbytes, _ := tokenizer.TagAttr()
							link = string(linkbytes)
						}
					case html.TextToken:
						if islink {
							links[link] = string(tokenizer.Text())
							islink = false
						}
					}
				}
				for url, _ := range links {
					if strings.Contains(url, "/watch?v=") {
						info, _ := ytdl.GetVideoInfo("https://www.youtube.com" + url)
						day_, _ := strconv.Atoi(channels_last_day[i])
						day_str := strconv.Itoa(info.DatePublished.YearDay() + info.DatePublished.Year()*1000)
						day_to0 := time.Now().YearDay() - day_ + time.Now().Year()*1000
						day_to1 := info.DatePublished.YearDay() + info.DatePublished.Year()*1000
						prov_ := time.Now().YearDay() - day_
						for prov_ < 0 {
							day_--
						}
						if info.Author == channels_name[i] && day_to1 >= day_to0 {
							fmt.Print(info.DatePublished.Format("02-01-2006"), "	", info.Title)
							format := info.Formats.Best("720p")[0]
							if channels_b_w[i] == "low" {
								format = info.Formats.Worst(ytdl.FormatResolutionKey)[0]
							}
							if channels_b_w[i] == "high" {
								format = info.Formats.Best("720p")[0]
							}
							os.Mkdir(info.Author, 0777)
							if _, err := os.Stat(info.Author + "/" + day_str + "_" + strings.Replace(info.Title, ":", " ", 99) + "_" + info.DatePublished.Format("02-01-2006") + "_[" + strings.Replace(url, "/watch?v=", "", 1) + "]_." + format.Extension); os.IsNotExist(err) {
								file, _ := os.Create(info.Author + "/" + day_str + "_" + strings.Replace(info.Title, ":", " ", 99) + "_" + info.DatePublished.Format("02-01-2006") + "_[" + strings.Replace(url, "/watch?v=", "", 1) + "]_." + format.Extension)
								//if null_file[i] != "yes" {
								if null_ != "null" {
									info.Download(format, file)
								}
							}
							fmt.Println(" ----- success")
						}
					}
				}
			}
		}
	}
}

func load_video(url string, title_ string, datepub string) {
	dlyt(url)
	//	info, _ := ytdl.GetVideoInfo("https://www.youtube.com/watch?v=" + url)
	//	const longForm = "2006-01-02T15:04:05.000Z"
	//	t, _ := time.Parse(longForm, datepub)
	//	format := info.Formats.Best("720p")[0]
	//	os.Mkdir(info.Author+"", 0777)
	//	filename_ := access_filename(info.Author) + "/" + t.Format("20060102") + "_" + access_filename(title_) + "_" + t.Format("02-01-2006") + "_[" + url + "]_." + format.Extension
	//	if _, err := os.Stat(filename_); os.IsNotExist(err) {
	//		file, _ := os.Create(filename_)
	//		info.Download(format, file)
	//	}
}

func video_link(url string) (string, string) {
	info, _ := ytdl.GetVideoInfo("https://www.youtube.com/watch?v=" + url)
	format := info.Formats.Best("720p")[0]
	link, _ := info.GetDownloadURL(format)
	return link.String(), info.Title
}

func files_in_dir() []os.FileInfo {
	files, _ := ioutil.ReadDir("./")
	return files
}

func access_filename(filename string) string {
	//	str_ := `*|\:"<>?/~@#$%^&`
	//	for i := 0; i < len(str_); i++ {
	//		filename = strings.Replace(filename, string(str_[i]), " ", -1)
	//	}
	//	return filename
	str_ := ` *|\:"<>?/~@#$%^&#●`
	for i := 0; i < len(str_); i++ {
		filename = strings.Replace(filename, string(str_[i]), "_", -1)
	}
	return filename
}

func delete_filename(path string) {
	//err := os.RemoveAll(path)
	//if err != nil {
	//	fmt.Println(err)
	//}
	defer os.RemoveAll(path)
}

func download_file(file_path string, download_path string, wg *sync.WaitGroup, bar *pb.ProgressBar) {
	resp, _ := http.Get(download_path)
	defer resp.Body.Close()
	out, _ := os.Create(file_path)
	defer out.Close()
	//---

	i, _ := strconv.Atoi(resp.Header.Get("Content-Length"))
	sourceSize := int64(i)
	bar.Total = sourceSize

	bar.Start()

	//bar := pb.ProgressBarTemplate(`{{ red "download:" }} {{ bar . "[" "█" "_" "_" "]"  | green }} {{percent . | rndcolor }}{{string . "suffix"}}`).Start(int(sourceSize))

	reader := bar.NewProxyReader(resp.Body)
	io.Copy(out, reader)
	//---
	//out, _ := os.Create(file_path)
	//defer out.Close()
	//io.Copy(out, resp.Body)
	wg.Done()
}

func download_file_yt(videoID string, quality_fhd bool) {

	inform, paths := VideoInformBest(videoID, quality_fhd)
	fndef := access_filename(inform.Author) + "/" + access_filename(inform.Title) + "_" + inform.ID

	filescheck, _ := ioutil.ReadDir(access_filename(inform.Author))
	for _, file := range filescheck {
		if strings.Contains(file.Name(), access_filename(inform.ID)) {
			return
		}
	}

	//----
	checkstat := 0
	fres := "def"
	for _, file := range paths {
		if file.Part && file.Type == "mp4" {
			if checkstat == 0 && file.Res != fres {
				file.Res = fres
			}
			checkstat++
		}
	}

	//----
	var wg sync.WaitGroup
	for _, file := range paths {
		wg.Add(1)
		if file.Res == "def" {
			os.Mkdir(access_filename(inform.Author), 0777)
			fn := fndef + "." + file.Type
			//---
			bar := pb.New(1).SetUnits(pb.U_BYTES).SetRefreshRate(time.Second * 3)
			bar.ShowSpeed = false
			bar.ShowCounters = false
			bar.ShowTimeLeft = false
			bar.ShowFinalTime = false
			bar.NotPrint = true
			defer bar.Finish()

			go download_file(fn, file.Path, &wg, bar)
			//---
			go func() {
				for {
					time.Sleep(time.Second * 3)
					go func() {
						//fmt.Println(bar.String())
						////chan_[videoID] = make(chan string)
						////chan_[videoID] <- bar.String()
						chan_[videoID] = bar.String()
					}()
				}
			}()
			//---
		} else {
			wg.Done()
		}
	}
	wg.Wait()

	if len(paths) > 1 {

		os.Rename(fndef+".mp4", fndef+"_part.mp4")
		os.Rename(fndef+".m4a", fndef+"_part.m4a")

		args_ := []string{"-i", fndef + "_part.mp4", "-i", fndef + "_part.m4a", "-codec", "copy", "-shortest", fndef + ".mp4"}
		if runtime.GOOS == "windows" {
			cmd := exec.Command("ffmpeg", args_...)
			cmd.Run()
		} else {
			cmd := exec.Command("./ffmpeg", args_...)
			cmd.Run()
		}
		delete_filename(fndef + "_part.mp4")
		delete_filename(fndef + "_part.m4a")

	}
}

func VideoInform(videoID string) (*VideoInfo, bool) {
	urldef, _ := url.ParseRequestURI(youtubeVideoInfoURL)
	values := urldef.Query()
	values.Set("video_id", videoID)
	urldef.RawQuery = values.Encode()
	resp, _ := http.Get(urldef.String())
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	//---
	if strings.Contains(string(body), "errorcode=150") {

		info_, _ := ytdl.GetVideoInfo(youtubeBaseURL + "?v=" + videoID)
		format0 := info_.Formats.Best("720p")[0]

		var formats = ytdl.FormatList{
			ytdl.FORMATS[137],
			ytdl.FORMATS[140],
		}

		type formatListAlter struct {
			Key             ytdl.FormatKey
			FilterValues    interface{}
			ExpectedFormats ytdl.FormatList
		}

		video_ := formatListAlter{ytdl.FormatItagKey, []interface{}{"137"}, ytdl.FormatList{formats[0]}}
		audio_ := formatListAlter{ytdl.FormatItagKey, []interface{}{"140"}, ytdl.FormatList{formats[1]}}

		format := info_.Formats.Filter(video_.Key, video_.FilterValues.([]interface{}))[0]
		format2 := info_.Formats.Filter(audio_.Key, audio_.FilterValues.([]interface{}))[0]
		info := &VideoInfo{}
		info.Title = access_filename(info_.Title)
		info.ID = videoID
		info.Author = access_filename(info_.Author)
		info.TimeStr = "(-)"
		val_0, _ := info_.GetDownloadURL(format0)
		val__0 := val_0.String()
		val_, _ := info_.GetDownloadURL(format)
		val__ := val_.String()
		val_2, _ := info_.GetDownloadURL(format2)
		val__2 := val_2.String()

		vvv0, _ := url.ParseQuery("")
		vvv0.Add("url", val__0)
		vvv0.Add("type", "video/mp4")
		vvv0.Add("quality_label", "720p")

		vvv, _ := url.ParseQuery("")
		vvv.Add("url", val__)
		vvv.Add("type", "video/mp4")
		vvv.Add("quality_label", "1080p")
		vvv.Add("url", val__2)
		vvv.Add("type", "audio/mp4")
		vvv.Add("quality_label", "audio")

		info.Paths = vvv0
		info.PathsAV = vvv
		return info, true
	}
	//---
	args_all, _ := url.ParseQuery(strings.Replace(string(body), "%2C", "%26", -1))
	var urls_full, urls_av url.Values
	if len_full := len(args_all["url_encoded_fmt_stream_map"]); len_full > 0 {
		urls_full, _ = url.ParseQuery(args_all["url_encoded_fmt_stream_map"][0])
	}
	if len_av := len(args_all["adaptive_fmts"]); len_av > 0 {
		urls_av, _ = url.ParseQuery(args_all["adaptive_fmts"][0])
	}
	//fmt.Println(urldef.String())
	//fmt.Println(args_all)
	author_ := args_all["author"][0]
	title_ := args_all["title"][0]
	length_seconds_ := args_all["length_seconds"][0]
	lenvid, _ := strconv.Atoi(length_seconds_)
	str_lenvid := "(" + strconv.Itoa(lenvid/60) + " min " + strconv.Itoa(lenvid%60) + " sec)"
	if len(urls_av["quality_label"]) != len(urls_av["url"]) {
		r := len(urls_av["url"]) - len(urls_av["quality_label"])
		for i := 0; i < r; i++ {
			urls_av.Add("quality_label", "audio")
		}
	}
	if len(urls_av["type"]) != len(urls_av["url"]) {
		r := len(urls_av["url"]) - len(urls_av["type"])
		for i := 0; i < r; i++ {
			urls_av.Add("type", "error")
		}
	}
	info := &VideoInfo{}
	info.Title = access_filename(title_)
	info.ID = videoID
	info.Author = access_filename(author_)
	info.TimeStr = str_lenvid
	info.Paths = urls_full
	info.PathsAV = urls_av
	return info, false
	//url_dl_a = urls_av["url"][i]
}

func VideoInformBest(videoID string, quality_fhd bool) (*VideoInfo, []*Paths) {
	info, alter := VideoInform(videoID)
	//---
	if alter {
		fmt.Println("alter used")

	}
	//---
	pathsfull := info.Paths
	paths := info.PathsAV
	paths_ := []*Paths{}
	flag_find_a, flag_find_v := false, false
	if quality_fhd {
		for i := 0; i < len(paths["url"]); i++ {
			par1 := paths["type"][i]
			par2 := paths["quality_label"][i]
			if (strings.Contains(par1, "video/mp4") && strings.Contains(par2, "1080p")) || (strings.Contains(par2, "audio") && strings.Contains(par1, "audio/mp4")) {
				if strings.Contains(par1, "audio/mp4") {
					//paths_ = append(paths_, paths["url"][i])
					inf := &Paths{}
					inf.Path = paths["url"][i]
					inf.Type = "m4a"
					inf.Part = true
					if strings.Contains(par2, "1080p60") {
						inf.Res = "60"
					} else {
						inf.Res = "def"
					}
					paths_ = append(paths_, inf)
					flag_find_a = true
				} else {
					//paths_ = append(paths_, paths["url"][i])
					inf := &Paths{}
					inf.Path = paths["url"][i]
					inf.Type = "mp4"
					inf.Part = true
					if strings.Contains(par2, "1080p60") {
						inf.Res = "60"
					} else {
						inf.Res = "def"
					}
					paths_ = append(paths_, inf)
					flag_find_v = true
				}
			}
		}
	}
	if !flag_find_a || !flag_find_v {
		inf := &Paths{}
		inf.Path = pathsfull["url"][0]
		inf.Type = "mp4"
		inf.Part = false
		inf.Res = "def"
		paths_ = append(paths_, inf)
		//paths_ = append(paths_, pathsfull["url"][0])
	}

	return info, paths_
}

func downloader_(url_dl string, filename_ string, filename_0 string) {
	if _, err := os.Stat(filename_); os.IsNotExist(err) {
		os.Create(filename_)
	}
	res, _ := http.Head(url_dl)
	maps := res.Header
	length, _ := strconv.Atoi(maps["Content-Length"][0])

	limit := 10
	limitone := 5
	koof := length / 50000000
	if koof > 2 {
		limit = 10 * koof
	}
	len_sub := length / limit
	diff := length % limit
	coun0 := 0
	coun := 0
	bar := pb.New(limit)
	bar.Start()
	var wg sync.WaitGroup
	for i := 0; i < limit; i++ {
		wg.Add(1)
		min := len_sub * i
		max := len_sub * (i + 1)
		if i == limit-1 {
			max += diff
		}
		coun0++
		go func(min int, max int, i int) {
			client := &http.Client{}
			req, _ := http.NewRequest("GET", url_dl, nil)
			range_header := "bytes=" + strconv.Itoa(min) + "-" + strconv.Itoa(max-1)
			req.Header.Add("Range", range_header)
			resp, _ := client.Do(req)
			reader, _ := ioutil.ReadAll(resp.Body)
			//defer resp.Body.Close()
			f, _ := os.Create(filename_0 + "_" + strconv.Itoa(i))
			if _, err := f.WriteString(string(reader)); err != nil {
				panic(err)
			}
			wg.Done()
			coun++
			bar.Add(1)
			resp.Body.Close()
		}(min, max, i)
		for {
			a := "" //WTF
			a = a + a
			if coun == limitone {
				coun0 = 0
				coun = 0
				break
			}
			if coun0 < limitone {
				break
			}
		}
	}
	wg.Wait()

	f, _ := os.OpenFile(filename_, os.O_APPEND|os.O_WRONLY, 0777)
	for i := 0; i < limit; i++ {
		f0, _ := os.OpenFile(filename_0+"_"+strconv.Itoa(i), os.O_RDONLY, 0777)
		//defer f.Close()
		reader, _ := ioutil.ReadAll(f0)
		if _, err := f.WriteString(string(reader)); err != nil {
			panic(err)
		}
		f0.Close()
		defer f0.Close()
		delete_filename(filename_0 + "_" + strconv.Itoa(i))
	}
	f.Close()

}

func dlyt(videoID string) {
	u0, _ := url.ParseRequestURI(youtubeVideoInfoURL)
	values0 := u0.Query()
	values0.Set("video_id", videoID)
	u0.RawQuery = values0.Encode()
	resp0, _ := http.Get(u0.String())
	defer resp0.Body.Close()
	body0, _ := ioutil.ReadAll(resp0.Body)
	//fmt.Println(string(body0))
	args_all, _ := url.ParseQuery(strings.Replace(string(body0), "%2C", "%26", -1))
	var urls_full, urls_av url.Values
	if len_full := len(args_all["url_encoded_fmt_stream_map"]); len_full > 0 {
		urls_full, _ = url.ParseQuery(args_all["url_encoded_fmt_stream_map"][0])
	}
	if len_av := len(args_all["adaptive_fmts"]); len_av > 0 {
		urls_av, _ = url.ParseQuery(args_all["adaptive_fmts"][0])
	}
	author_ := args_all["author"][0]
	title_ := args_all["title"][0]
	length_seconds_ := args_all["length_seconds"][0]
	lenvid, _ := strconv.Atoi(length_seconds_)
	str_lenvid := "(" + strconv.Itoa(lenvid/60) + " min " + strconv.Itoa(lenvid%60) + " sec)"
	if len(urls_av["quality_label"]) != len(urls_av["url"]) {
		r := len(urls_av["url"]) - len(urls_av["quality_label"])
		for i := 0; i < r; i++ {
			urls_av.Add("quality_label", "audio")
		}
	}
	if len(urls_av["type"]) != len(urls_av["url"]) {
		r := len(urls_av["url"]) - len(urls_av["type"])
		for i := 0; i < r; i++ {
			urls_av.Add("type", "error")
		}
	}
	info := &VideoInfo{}
	info.Title = title_
	info.ID = videoID
	info.Author = author_
	info.TimeStr = str_lenvid
	flag_find_v := 0
	flag_find_a := 0
	url_dl := ""
	url_dl_a := ""
	filename_0 := ""
	filename_ := ""
	filename_0_a := ""
	filename_a := ""
	os.Mkdir(access_filename(info.Author), 0777)
	os.Mkdir(access_filename(info.Author)+"/tmp"+access_filename(info.Title), 0777)
	println(access_filename(info.Author) + "/tmp" + access_filename(info.Title))
	for i := 0; i < len(urls_av["url"]); i++ {
		//fmt.Println(string(urls_av), len(urls_av["type"]), len(urls_av["type"]))
		par1 := urls_av["type"][i]
		par2 := urls_av["quality_label"][i]
		if (strings.Contains(par1, "video/mp4") && strings.Contains(par2, "1080p")) || (strings.Contains(par2, "audio") && strings.Contains(par1, "audio/mp4")) {
			if strings.Contains(par1, "audio/mp4") {
				url_dl_a = urls_av["url"][i]
				filename_a = access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "m4a"
				filename_0_a = access_filename(info.Author) + "/tmp" + access_filename(info.Title) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "m4a"
				flag_find_a = 1
			} else {
				url_dl = urls_av["url"][i]
				filename_ = access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4"
				filename_0 = access_filename(info.Author) + "/tmp" + access_filename(info.Title) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4"
				flag_find_v = 1
			}
		}
	}
	if flag_find_a != 1 || flag_find_v != 1 {
		url_dl = urls_full["url"][0]
		filename_ = access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4"
		filename_0 = access_filename(info.Author) + "/tmp" + access_filename(info.Title) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4"
		////fmt.Println(access_filename(info.Title), "(full)")
		downloader_(url_dl, filename_, filename_0)
	} else {
		////fmt.Println("\n", access_filename(info.Title), "(video)")
		downloader_(url_dl, filename_, filename_0)
		////fmt.Println("\n", access_filename(info.Title), "(audio)")
		downloader_(url_dl_a, filename_a, filename_0_a)
	}
	args_ := []string{"-i", access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4", "-i", access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "m4a", "-codec", "copy", "-shortest", access_filename(info.Author) + "/" + access_filename(info.Title) + "__" + info.TimeStr + "." + "mp4"}
	if runtime.GOOS == "windows" {
		cmd := exec.Command("ffmpeg", args_...)
		cmd.Run()
		//cmd.Process.Kill()
	} else {
		cmd := exec.Command("./ffmpeg", args_...)
		cmd.Run()
		//cmd.Process.Kill()
	}
	if flag_find_a == 1 && flag_find_v == 1 {
		delete_filename(access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "mp4")
		delete_filename(access_filename(info.Author) + "/" + access_filename(info.Title) + "_" + info.TimeStr + "." + "m4a")
	}
	delete_filename(access_filename(info.Author) + "/tmp" + access_filename(info.Title))
	////fmt.Println("Complete: ", filename_)
}
func getIdVid(text string) string {
	newtext := text
	if strings.Contains(text, "youtu.be/") {
		if strings.Contains(text, "?") {
			newtext = strings.Split(text, "?")[0]
			newtext = strings.Split(newtext, "youtu.be/")[1]
		} else {
			newtext = strings.Split(text, "youtu.be/")[1]
		}
	}
	if strings.Contains(text, "watch?v=") {
		if strings.Contains(text, "&") {
			newtext = strings.Split(text, "&")[0]
			newtext = strings.Split(newtext, "watch?v=")[1]
		} else {
			newtext = strings.Split(text, "watch?v=")[1]
		}
	}

	return newtext
}

func telega() {
	// подключаемся к боту с помощью токена
	//	bot, err := tgbotapi.NewBotAPI(keytel)
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//	//bot.Debug = true

	//	//log.Printf("Authorized on account %s", bot.Self.UserName)

	//	u := tgbotapi.NewUpdate(0)
	//	u.Timeout = 60

	//	updates, err := bot.GetUpdatesChan(u)

	//	//time.Sleep(time.Millisecond * 500)

	//	for update := range updates {

	//		if update.Message == nil {
	//			break
	//		}

	//		text01 := `https://youtu.be/`
	//		text02 := `https://www.youtube.com/watch?v=`

	//		if !strings.Contains(update.Message.Text, text01) && !strings.Contains(update.Message.Text, text02) {
	//			break
	//		}

	//		urlstr := strings.Replace(update.Message.Text, text01, "", 1)
	//		urlstr = strings.Replace(urlstr, text02, "", 1)
	//		info_, _ := ytdl.GetVideoInfo(urlstr)
	//		format0 := info_.Formats.Best("720p")[0]
	//		val_0, _ := info_.GetDownloadURL(format0)
	//		val__0 := val_0.String()

	////log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

	//response, _ := http.Get(`http://tinyurl.com/api-create.php?url=` + val__0)
	//defer response.Body.Close()
	//contents_, _ := ioutil.ReadAll(response.Body)

	//msg := tgbotapi.NewMessage(update.Message.Chat.ID, string(contents_))

	//data, _ := ioutil.ReadFile(info_.GetThumbnailURL(ytdl.ThumbnailQualityMedium).String())

	// -  /getMe
	// {"ok":true,"result":{"id":382977846,"first_name":"ytlinkbot","username":"ytlinkbot"}}

	//req, _ := http.NewRequest("POST", "https://api.telegram.org/bot"+keytel+"/getUpdates?offset=0", nil)
	//client := http.Client{}
	//resp, _ := client.Do(req)
	//data, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println(string(data))

	//curl -i -X GET https://api.telegram.org/bot<apikey>/sendMessage?chat_id=<chatId>&text=<someText>

	//req, _ = http.NewRequest("POST", "https://api.telegram.org/bot"+keytel+"/sendMessage?chat_id=237722513&text=test", nil)
	//client = http.Client{}
	//resp, _ = client.Do(req)
	//data, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println(string(data))

	//b := tgbotapi.FileBytes{Name: "image.jpg", Bytes: data}

	//msg := tgbotapi.NewPhotoUpload(update.Message.Chat.ID, b)

	//msg := tgbotapi.NewMessage(update.Message.Chat.ID, info_.GetThumbnailURL(ytdl.ThumbnailQualityMedium).String())

	//msg := tgbotapi.NewMessage(update.Message.Chat.ID, `<a href="`+val__0+`">`+info_.Title+`</a><a href="`+strings.Replace(info_.GetThumbnailURL(ytdl.ThumbnailQualityDefault).String(), "jpg?", "jpg", 1)+`">pic</a>`)
	////msg.ReplyToMessageID = update.Message.MessageID

	//msg.ParseMode = "HTML"

	//bot.Send(msg)

	//}
	///-------------------
	//	bot, err := tgbotapi.NewBotAPI(keytel)

	//	var netTransport = &http.Transport{
	//		Dial: (&net.Dialer{
	//			Timeout: 5 * time.Second,
	//		}).Dial,
	//		TLSHandshakeTimeout: 5 * time.Second,
	//	}
	//	var netClient = &http.Client{
	//		Timeout:   time.Second * 10,
	//		Transport: netTransport,
	//	}

	//	bot.Client = netClient
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//	bot.Debug = true
	//-------------
	//req, _ := http.NewRequest("POST", "https://api.telegram.org/bot"+keytel+"/sendMessage?chat_id=237722513&text=test2", nil)
	//client := http.Client{}
	//client.Do(req)

	//-------------------
	//	for {
	//		u := tgbotapi.NewUpdate(0)
	//		u.Timeout = 60

	//		updates, _ := bot.GetUpdates(u)

	//		time.Sleep(time.Second * 3)
	//		for i := 0; i < len(updates); i++ {

	//			reset := strconv.Itoa(updates[i].UpdateID)
	//			req, _ := http.NewRequest("POST", "https://api.telegram.org/bot"+keytel+"/getUpdates?offset="+reset, nil)
	//			client := http.Client{}
	//			client.Do(req)

	//			if updates[i].Message == nil {
	//				continue
	//			}

	//			text01 := `https://youtu.be/`
	//			text02 := `https://www.youtube.com/watch?v=`

	//			if !strings.Contains(updates[i].Message.Text, text01) && !strings.Contains(updates[i].Message.Text, text02) {

	//				continue
	//			}

	//			urlstr := ""
	//			if strings.Contains(updates[i].Message.Text, text01) {
	//				urlstr = strings.Split(updates[i].Message.Text, text01)[1]
	//			}
	//			if strings.Contains(updates[i].Message.Text, text02) {
	//				urlstr = strings.Split(updates[i].Message.Text, text02)[1]
	//			}
	//			info_, _ := ytdl.GetVideoInfo(urlstr)
	//			format0 := info_.Formats.Best("720p")[0]
	//			val_0, _ := info_.GetDownloadURL(format0)
	//			val__0 := `<a href="` + val_0.String() + `">` + info_.Title + `</a>`

	//			msg := tgbotapi.NewMessage(updates[i].Message.Chat.ID, val__0)

	//			msg.ParseMode = "HTML"

	//			bot.Send(msg)

	//			reset = strconv.Itoa(updates[i].UpdateID + 1)
	//			req, _ = http.NewRequest("POST", "https://api.telegram.org/bot"+keytel+"/getUpdates?offset="+reset, nil)
	//			client = http.Client{}
	//			client.Do(req)

	//		}
	//	}

	bot, err := tgbotapi.NewBotAPI(keytel)
	if err != nil {
		fmt.Println(err)
	}
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	bot.Debug = false
	for update := range updates {
		if update.Message == nil {
			continue
		}

		//text01 := `https://youtu.be/`
		//text02 := `https://www.youtube.com/watch?v=`

		//if !strings.Contains(update.Message.Text, text01) && !strings.Contains(update.Message.Text, text02) {
		//	continue
		//}

		info_, errvid := ytdl.GetVideoInfo(getIdVid(update.Message.Text))
		if errvid != nil || info_.Author == "" {
			continue
		}

		//urlstr := strings.Replace(update.Message.Text, text01, "", 1)
		//urlstr = strings.Replace(urlstr, text02, "", 1)
		//info_, _ := ytdl.GetVideoInfo(urlstr)
		//format0 := info_.Formats.Best("720p")[0]
		//val_0, _ := info_.GetDownloadURL(format0)
		//val__0 := `<a href="` + val_0.String() + `">` + info_.Title + `</a>`

		req, _ := http.NewRequest("GET", info_.GetThumbnailURL(ytdl.ThumbnailQualityMedium).String(), nil)
		client := http.Client{}
		resp, _ := client.Do(req)
		data, _ := ioutil.ReadAll(resp.Body)
		b := tgbotapi.FileBytes{Name: "image.jpg", Bytes: data}

		msg := tgbotapi.NewPhotoUpload(update.Message.Chat.ID, b)
		bot.Send(msg)

		var formats = ytdl.FormatList{
			ytdl.FORMATS[36],
			ytdl.FORMATS[18],
			ytdl.FORMATS[22],
			ytdl.FORMATS[140],
		}

		type formatListAlter struct {
			Key             ytdl.FormatKey
			FilterValues    interface{}
			ExpectedFormats ytdl.FormatList
		}

		video_240 := formatListAlter{ytdl.FormatItagKey, []interface{}{"36"}, ytdl.FormatList{formats[0]}}
		video_360 := formatListAlter{ytdl.FormatItagKey, []interface{}{"18"}, ytdl.FormatList{formats[1]}}
		video_720 := formatListAlter{ytdl.FormatItagKey, []interface{}{"22"}, ytdl.FormatList{formats[2]}}
		audio_ := formatListAlter{ytdl.FormatItagKey, []interface{}{"140"}, ytdl.FormatList{formats[3]}}

		format240 := info_.Formats.Filter(video_240.Key, video_240.FilterValues.([]interface{}))[0]
		format360 := info_.Formats.Filter(video_360.Key, video_360.FilterValues.([]interface{}))[0]
		format720 := info_.Formats.Filter(video_720.Key, video_720.FilterValues.([]interface{}))[0]
		formataudio := info_.Formats.Filter(audio_.Key, audio_.FilterValues.([]interface{}))[0]

		v240_, _ := info_.GetDownloadURL(format240)
		v240 := `<a href="` + v240_.String() + `">` + info_.Title + `[worst]</a>` + "\n"
		v360_, _ := info_.GetDownloadURL(format360)
		v360 := `<a href="` + v360_.String() + `">` + info_.Title + `[good]</a>` + "\n"
		v720_, _ := info_.GetDownloadURL(format720)
		v720 := `<a href="` + v720_.String() + `">` + info_.Title + `[best]</a>` + "\n"
		vau_, _ := info_.GetDownloadURL(formataudio)
		vau := `<a href="` + vau_.String() + `">` + info_.Title + `[audio]</a>` + "\n"

		msg1 := tgbotapi.NewMessage(update.Message.Chat.ID, v240+v360+v720+vau)
		msg1.ParseMode = "HTML"

		//fmt.Println(v240, v360, v720, vau)

		bot.Send(msg1)

		reqa, _ := http.NewRequest("GET", vau_.String(), nil)
		clienta := http.Client{}
		respa, _ := clienta.Do(reqa)
		dataa, _ := ioutil.ReadAll(respa.Body)
		//ba := tgbotapi.FileBytes{Name: "audio.m4a", Bytes: dataa}
		ba := tgbotapi.FileBytes{Name: info_.Title + ".m4a", Bytes: dataa}

		msg2 := tgbotapi.NewAudioUpload(update.Message.Chat.ID, ba)
		//msg2 := tgbotapi.NewVideoUpload(update.Message.Chat.ID, ba)
		//msg2 := tgbotapi.NewAudioShare(update.Message.Chat.ID, ba)
		bot.Send(msg2)
		//бить по 50 мегобайт
		//https://tlgrm.ru/docs/bots/api#sendaudio
	}

}

func Download__(w http.ResponseWriter, req *http.Request) string {
	loadonserv := false
	loadonserv1080 := false
	loadlist := false
	body, _ := ioutil.ReadAll(req.Body)
	v, _ := url.ParseQuery(string(body))
	for key, _ := range v {
		if key == "plus" {
			loadonserv = true
		}
		if key == "plus1080" {
			loadonserv1080 = true
		}
		if key == "dlist" {
			loadlist = true
		}
	}
	for key_, value := range v {
		if loadonserv && key_ != "plus" && key_ != "plus1080" && key_ != "dlist" {
			go download_file_yt(value[0], false)
			http.Redirect(w, req, "/", 302)
			////inform, _ := VideoInformBest(value[0], false)
			//				inform := info_video(value[0], key)

			//				go load_video(value[0], inform.Items[0].Snippet.Title, inform.Items[0].Snippet.PublishedAt)
			//				http.Redirect(w, req, "/", 302)
		}
		if loadonserv1080 && key_ != "plus" && key_ != "plus1080" && key_ != "dlist" {
			go download_file_yt(value[0], true)
			//go load_video(value[0], inform.Items[0].Snippet.Title, inform.Items[0].Snippet.PublishedAt)
			http.Redirect(w, req, "/", 302)
		}
		if loadlist && key_ != "plus1080" && key_ != "plus" && key_ != "dlist" {

			var newOnePage PageItem
			var item_ []ItemVideo
			zn1, zn2 := video_link(value[0])
			var newOneVideo ItemVideo
			newOneVideo.Value = zn2
			newOneVideo.UrlImg = zn1
			item_ = append(item_, newOneVideo)
			check := func(err error) {
				if err != nil {
					fmt.Println(err)
				}
			}
			newOnePage.Item = item_
			buf := new(bytes.Buffer)
			t, err := template.New("link_list.tpl").ParseFiles(`settings/tpl/link_list.tpl`)
			check(err)
			err = t.Execute(buf, newOnePage)
			check(err)
			return buf.String()
		}
	}
	return ""

}

//тут сделать отпраку 100 если больше 100 и на клиенте если 100 то значит прекратить опрос. И сделать опрос при загрузке автора и сразу всех чекнутых. Может сделать передачу сразу массива в json, а может оставить так. Json передавать выгоднее т.к. в секунду будет всего 1 запрос.
//person = '/*-secure-\n{"name": "Violet", "occupation": "character"}\n*/'.evalJSON()
//person.name;

//new Ajax.Request('/some_url', {
//  method:'get',
//  requestHeaders: {Accept: 'application/json'},
//  onSuccess: function(transport){
//    var json = transport.responseText.evalJSON(true); //sanitize
//  }
//});
func Download_range(w http.ResponseWriter, req *http.Request) string {
	body, _ := ioutil.ReadAll(req.Body)
	v, _ := url.ParseQuery(string(body))
	for key_, value := range v {
		//fmt.Println(key_, value)
		if key_ == "procent" {
			msg := chan_[value[0]]
			//msg_, _ := strconv.Atoi(msg)
			//msg = strconv.Itoa(msg_ + 1)
			//fmt.Println(msg)
			//msg_ := msg
			return msg
		}
	}
	return ""
}

func channel_id(input string) string {
	const (
		repcheck_ch    = "/channel/"
		repcheck_us    = "/user/"
		repcheck_after = "/"
	)
	var ok bool
	var idorch, idorchfinal []string
	var id string
	input_ := strings.Replace(input, " ", "", -1)

	check := func(str string, substr string) ([]string, bool) {
		if strings.Contains(str, substr) {
			idorch = strings.Split(str, substr)
			return idorch, true
		}
		return idorch, ok
	}

	if strings.Contains(input_, repcheck_after) {

		idorch, ok = check(input_, repcheck_ch)
		idorch, ok = check(input_, repcheck_us)
		if ok {
			ok = false
			id = idorch[1]
			idorchfinal, ok = check(idorch[1], repcheck_after)
			if ok {
				id = idorchfinal[0]
			}
		} else {
			idorchfinal, ok = check(input_, repcheck_after)
			if ok {
				id = idorchfinal[0]
			} else {
				id = ""
			}
		}
		return id
	} else {
		return input_
	}

}

func Runch__(w http.ResponseWriter, req *http.Request) string {
	if _, err := os.Stat(namefilech); os.IsNotExist(err) {
		write_channels_file()
	}
	pos := 0

	var newOnePage PageItem
	var item_ []ItemVideo
	var channels string

	for i := 0; i < len(read_channels()); i++ {
		channels += "@@@" + read_channels()[i] + "@@@" //ЗПТ
	}
	channels = strings.Replace(channels, "@@@@@@", "%2C", -1)
	channels = strings.Replace(channels, "@@@", "", -1)
	info_ := info_ch(channels).Channels
	for i := 0; i < len(read_channels()); i++ {
		pos += 350
		var newOneVideo ItemVideo
		newOneVideo.Pos = strconv.Itoa(pos)
		newOneVideo.Value = info_[i].Title
		newOneVideo.Id = info_[i].Id
		newOneVideo.Ico = info_[i].Ico
		newOneVideo.Subs = info_[i].Subscribers
		item_ = append(item_, newOneVideo)
	}
	check := func(err error) {
		if err != nil {
			fmt.Println(err)
		}
	}
	newOnePage.Item = item_
	buf := new(bytes.Buffer)
	t, err := template.New("author_list.tpl").ParseFiles(`settings/tpl/author_list.tpl`)
	check(err)
	err = t.Execute(buf, newOnePage)
	check(err)
	return buf.String()
}

func Run__(req *http.Request) string {
	inidl := ""
	//def_count__ := strconv.Itoa(def_count_)
	//pageonload_ := pageonload
	var item_ []ItemVideo
	body, _ := ioutil.ReadAll(req.Body)
	//fmt.Println(string(body))
	v, _ := url.ParseQuery(string(body))
	for key, value := range v {

		if key == "fileini" {
			inidl = value[0]
		}
		if key == "count" {
			def_count_ = value[0]
		}
		if key == "count_page" {
			pageonload, _ = strconv.Atoi(value[0])
		}
	}
	var res QueryV
	nextp := ""
	count_page := 1
	//		count_page := res.PageInfo.TotalResults / res.PageInfo.ResultsPerPage
	//		last_page := res.PageInfo.TotalResults % res.PageInfo.ResultsPerPage
	//		if last_page > 0 {
	//			count_page++
	//		}
	//		if pageonload > 0 && count_page > 1 {
	//			count_page = pageonload
	//		}
	// т.к. перенёс всё в 1 цикл, то нужно высчитывать либо заранее либо прямо в цикле наподобие while
	// и в цикле эти страницы высчитывать //тут
	i := 0

	for i < count_page {
		val1 := info_ch(inidl).Channels[0].Upload
		restmp := videos(val1, nextp, def_count_)

		//---
		count_page = restmp.PageInfo.TotalResults / restmp.PageInfo.ResultsPerPage
		last_page := restmp.PageInfo.TotalResults % restmp.PageInfo.ResultsPerPage
		if last_page > 0 {
			count_page++
		}
		if pageonload > 0 && count_page >= pageonload {
			count_page = pageonload
		}
		//---
		nextp = restmp.NextPageToken
		for i := 0; i < len(restmp.Items); i++ {
			res.Items = append(res.Items, restmp.Items[i])
		}
		i++
	}
	ccc := len(res.Items)
	const longForm = "2006-01-02T15:04:05.000Z"
	items := []Item_box{}
	box := Items_box{items}

	var newOnePage PageItem
	newOnePage.IcoGeneral = info_ch(inidl).Channels[0].Ico

	for i := 0; i < ccc; i++ {
		t, _ := time.Parse(longForm, res.Items[i].Snippet.PublishedAt)
		checked := ""
		files, _ := ioutil.ReadDir(access_filename(res.Items[i].Snippet.ChannelTitle))
		for _, file := range files {
			//fmt.Println(file.Name(),access_filename(res.Items[i].Snippet.ResourceId.VideoId))
			if strings.Contains(file.Name(), access_filename(res.Items[i].Snippet.ResourceId.VideoId)) {
				checked = "checked"
			}
		}
		var newOneVideo ItemVideo
		newOneVideo.Title = res.Items[i].Snippet.Title
		newOneVideo.UrlImg = res.Items[i].Snippet.Thumbnails.Quality_Item.Url
		newOneVideo.Checked = checked
		newOneVideo.Name = res.Items[i].Snippet.ResourceId.VideoId
		newOneVideo.Time = t.Format("02-01-2006[15:04]")
		newOneVideo.Value = res.Items[i].Snippet.ResourceId.VideoId
		item_ = append(item_, newOneVideo)
		//fmt.Println(checked)
	}
	newOnePage.Item = item_
	sort.Sort(sort.Reverse(box))
	check := func(err error) {
		if err != nil {
			fmt.Println(err)
		}
	}

	buf := new(bytes.Buffer)
	t, err := template.New("author.tpl").ParseFiles(`settings/tpl/author.tpl`)
	check(err)
	err = t.Execute(buf, newOnePage)
	check(err)

	return buf.String()
}

func Default__() string {
	if _, err := os.Stat(namefilech); os.IsNotExist(err) {
		write_channels_file()
	}

	var newOnePage PageItem
	var item_ []ItemVideo

	check := func(err error) {
		if err != nil {
			fmt.Println(err)
		}
	}

	newOnePage.Item = item_
	buf := new(bytes.Buffer)
	t, err := template.New("def.tpl").ParseFiles(`settings/tpl/def.tpl`)
	check(err)
	err = t.Execute(buf, newOnePage)
	check(err)
	return buf.String()
}

func EditCh__(w http.ResponseWriter, req *http.Request) {
	if _, err := os.Stat(namefilech); os.IsNotExist(err) {
		write_channels_file()
	}
	inidl := ""
	inidl_hide := ""
	body, _ := ioutil.ReadAll(req.Body)
	//fmt.Println(string(body))
	v, _ := url.ParseQuery(string(body))
	for key, value := range v {
		if key == "channel_name" {
			inidl = name_or_id(channel_id(value[0]))
		}
		if key == "channel_name_hide" {
			inidl_hide = value[0]
		}
	}
	val1 := info_ch(inidl).Channels[0].Upload
	if val1 != "" {
		del_channel(inidl, inidl_hide)
	}
	http.Redirect(w, req, "/", 302)
}

func AddCh__(w http.ResponseWriter, req *http.Request) {
	if _, err := os.Stat(namefilech); os.IsNotExist(err) {
		write_channels_file()
	}
	inidl := ""
	body, _ := ioutil.ReadAll(req.Body)
	//fmt.Println(string(body))
	v, _ := url.ParseQuery(string(body))
	for key, value := range v {
		if key == "channel_name" {
			inidl = name_or_id(channel_id(value[0]))
		}
	}
	val1 := info_ch(inidl).Channels[0].Upload
	if val1 != "" {
		add_channel(inidl)
	}
	http.Redirect(w, req, "/", 302)
}

func indexOf(arr []interface{}, index int, errorDef interface{}) (interface{}, error) {
	if len(arr)-1 < index {
		return errorDef, errors.New("over index")
	} else {
		return arr[index], nil
	}
}

func indexOfs(arr []string, index int, errorDef string) (string, error) {
	if len(arr)-1 < index {
		return errorDef, errors.New("over index")
	} else {
		if arr[index] == "" {
			return errorDef, nil
		}
		return arr[index], nil
	}
}

func printError(err error) {
	if err != nil {
		fmt.Println(err)
		//fmt.Errorf("Error detected: %s", err.Error())
	}
}

func testpb(bar *pb.ProgressBar) {

	for i := 0; i < 10; i++ {
		bar.Add(1)
		time.Sleep(time.Second * 1)
	}
	bar.Finish()
}

func testpbcur(bar *pb.ProgressBar) {

	for i := 0; i < 10; i++ {
		bar.Add(1)
		time.Sleep(time.Second * 1)
	}
}

func main() {

	//bar := pb.New(10)
	//bar := pb.ProgressBarTemplate(`{{ red "download:" }} {{ bar . "[" "█" "_" "_" "]"  | green }} {{percent . | rndcolor }}{{string . "suffix"}}`).Start(10)
	//defer bar.Finish()
	//bar.Start()
	//go testpb(bar)

	//for i := 0; i < 10; i++ {
	//	time.Sleep(time.Second * 3)
	//fmt.Println(bar.Current())
	//}

	runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.LockOSThread()

	f, err := os.OpenFile("logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("error opening file log: %v", err)
	}
	defer f.Close()

	fmt.Println(`use "new" as argument for rewrite settings.`)

	var port__ int
	var web__ bool
	var bot__ bool
	var new_settings string
	var renew_settings bool

	new_settings, _ = indexOfs(os.Args, 1, "") //"new" or not
	if _, err := os.Stat(ini_file); os.IsNotExist(err) {
		renew_settings = true
	}

	if new_settings == "new" || renew_settings {
		fmt.Println("Hello. You are using Youtube download server. It's first run or you want change new settings. ")
		fmt.Print("Enter default port for Web (use digits): ")
		if _, err := fmt.Scan(&port__); err != nil {
			fmt.Println("error input data(%s), please try again", err)
		}
		fmt.Print("Use Web download on server (1-true/0-false): ")
		if _, err := fmt.Scan(&web__); err != nil {
			fmt.Println("error input data(%s), please try again", err)
		}
		fmt.Print("Use Telegram Bot (1-true/0-false): ")
		if _, err := fmt.Scan(&bot__); err != nil {
			fmt.Println("error input data(%s), please try again", err)
		}
		fmt.Print("Enter google-api code: ")
		if _, err := fmt.Scan(&key); err != nil {
			fmt.Println("error input data(%s), please try again", err)
		}
		fmt.Print("Enter telegram api code: ")
		if _, err := fmt.Scan(&keytel); err != nil {
			fmt.Println("error input data(%s), please try again", err)
		}

		text_ := strconv.Itoa(port__) + "\n" + strconv.FormatBool(web__) + "\n" + strconv.FormatBool(bot__) + "\n" + key + "\n" + keytel
		d1 := []byte(text_)
		ioutil.WriteFile(ini_file, d1, 0777)

	}

	dat, _ := ioutil.ReadFile(ini_file)
	sett := strings.Split(string(dat), "\n")

	m := martini.Classic()
	m.Map(log.New(f, "[martini]", log.LstdFlags))

	port_, _ := indexOfs(sett, 0, "1234")
	web_, _ := indexOfs(sett, 1, "true")
	bot_, _ := indexOfs(sett, 2, "false")
	key, _ = indexOfs(sett, 3, "")
	keytel, _ = indexOfs(sett, 4, "")

	if web_ == "true" {
		m.Post("/download", Download__)
		m.Post("/download_percent", Download_range)
		m.Post("/run", Run__)
		m.Post("/runch", Runch__)
		m.Post("/add_ch", AddCh__)
		m.Post("/edit_ch", EditCh__)
		m.Get("/", Default__)
		m.Use(martini.Static("settings"))
		http.Handle("/", m)
		fmt.Println("web is running...")
	} else {
		fmt.Println("web is not running")
	}

	if bot_ == "true" {
		go telega()
		fmt.Println("bot is running...")
	} else {
		fmt.Println("bot is not running")
	}

	m.RunOnAddr(":" + port_)
	//	msg := <-chan_["test"]
	//	fmt.Println(msg)
}
